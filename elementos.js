function crear_elemento( elemento, atributos )
{
    var obj_temporal = document.createElement( elemento );
    
    for( var i = 0; i < atributos.length; i++ )
    {
        if( atributos[i]["name"] === "textContent" )
            obj_temporal.textContent = atributos[i]["value"]
        obj_temporal.setAttribute( atributos[i]["name"], atributos[i]["value"] );
    }
    return obj_temporal;
}

function crear_select( atributos, opciones )
{
    var obj_temporal = document.createElement( "select" );
    
    for( var i = 0; i < atributos.length; i++ )
        obj_temporal.setAttribute( atributos[i]["name"], atributos[i]["value"] );
    for( i = 0; i < opciones.length; i++ )
    {
        var obj_opcion;
        obj_opcion = crear_elemento( "option", opciones[i] );
        obj_temporal.appendChild( obj_opcion );
    }
    return obj_temporal;
}

function asociar_hijo( hijo, padre )
{
    padre.appendChild( hijo );
}

div_container = crear_elemento( "div", {"name" : "class", "value" : "container" } );
form = crear_elemento( "form", { "name" : "class", "value" : "navbar-form navbar-left" } );
div_form = crear_elemento( "div", {"name" : "class", "value" : "form-group" } );
div_fila1 = crear_elemento( "div", {"name" : "class", "value" : "row" } );
div_col11 = crear_elemento( "div", {"name" : "class", "value" : "col-xs-6" } );
label_nombre = crear_elemento( "label", [ {"name" : "class", "value" : "" }, {"name" : "textContent", 
"value" : "Nombre" } ] );
div_col12 = crear_elemento( "div", { "name" : "class", "value" : "col-xs-6" } );
input_nombre = crear_elemento( "input", [ {"name" : "class", "value" : "form-control" }, {"name" : "type", "value" : "text" }, { "name" : "placeholder", "value" : "Nombre" } ] );

div_fila2 = crear_elemento( "div", {"name" : "class", "value" : "row" } );
div_col21 = crear_elemento( "div", {"name" : "class", "value" : "col-xs-6" } );
label_edad = crear_elemento( "label", [ {"name" : "class", "value" : "" }, {"name" : "textContent", 
"value" : "Edad" } ] );
div_col22 = crear_elemento( "div", { "name" : "class", "value" : "col-xs-6" } );
input_edad = crear_elemento( "input", [ {"name" : "class", "value" : "form-control" }, {"name" : "type", "value" : "text" }, { "name" : "placeholder", "value" : "Edad" } ] );

div_fila3 = crear_elemento( "div", {"name" : "class", "value" : "row" } );
div_col31 = crear_elemento( "div", {"name" : "class", "value" : "col-xs-6" } );
label_sexo = crear_elemento( "label", [ {"name" : "class", "value" : "" }, {"name" : "textContent", 
"value" : "Sexo" } ] );
div_col32 = crear_elemento( "div", { "name" : "class", "value" : "col-xs-6" } );
atributos_select = 
    [
        { "name" : "id", "value" : "select" }
    ];

atributos_option =
    [
        [
            { "name" : "id", "value" : "op1" },
            { "name" : "value", "value" : "H" },
            { "name" : "textContent", "value" : "Hombre" }
        ],
        [
            { "name" : "id", "value" : "op2" },
            { "name" : "value", "value" : "M" },
            { "name" : "textContent", "value" : "Mujer" }
        ]
    ];
select = crear_select( atributos_select, atributos_option );

div_fila4 = crear_elemento( "div", {"name" : "class", "value" : "row" } );
div_col41 = crear_elemento( "div", {"name" : "class", "value" : "col-xs-6" } );
label_observaciones = crear_elemento( "label", [ {"name" : "class", "value" : "" }, {"name" : "textContent", "value" : "Observaciones" } ] );
div_col42 = crear_elemento( "div", { "name" : "class", "value" : "col-xs-6" } );
textArea_observaciones = crear_elemento( "textarea", [ {"name" : "class", "value" : "form-control" }, {"name" : "type", "value" : "text-area" } ] );

div_fila5 = crear_elemento( "div", {"name" : "class", "value" : "row" } );
boton = crear_elemento( "input", [ {"name" : "class", "value" : "btn btn-default" }, {"name" : "value", "value" : "Guardar" }, {"name" : "type", "value" : "submit" } ] );


asociar_hijo( div_container, document.getElementsByTagName("body")[0] );
asociar_hijo( form, div_container );
asociar_hijo( div_form, form );


asociar_hijo( div_fila1, div_form );
asociar_hijo( div_col11, div_fila1 );
asociar_hijo( label_nombre, div_col11 );
asociar_hijo( div_col12, div_fila1 );
asociar_hijo( input_nombre, div_col12 );

asociar_hijo( div_fila2, div_form );
asociar_hijo( div_col21, div_fila2 );
asociar_hijo( label_edad, div_col21 );
asociar_hijo( div_col22, div_fila2 );
asociar_hijo( input_edad, div_col22 );

asociar_hijo( div_fila3, div_form );
asociar_hijo( div_col31, div_fila3 );
asociar_hijo( label_sexo, div_col31 );
asociar_hijo( div_col32, div_fila3 );
asociar_hijo( select, div_col32 );

asociar_hijo( div_fila4, div_form );
asociar_hijo( div_col41, div_fila4 );
asociar_hijo( label_observaciones, div_col41 );
asociar_hijo( div_col42, div_fila4 );
asociar_hijo( textArea_observaciones, div_col42 );

asociar_hijo( div_fila5, div_form );
asociar_hijo( boton, div_fila5 );
